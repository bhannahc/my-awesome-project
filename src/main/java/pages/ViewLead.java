package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="viewLead_firstName_sp")
	WebElement eleFname;
	public ViewLead verifyFName(String expectedText)
	{
		verifyExactText(eleFname, expectedText);
		return this;
	}
	
	@FindBy(how=How.LINK_TEXT,using="Edit")
	WebElement eEdit;
	public void editData()
	{
	click(eEdit);	
	}
	
	@FindBy(how=How.ID,using="viewLead_companyName_sp")
	WebElement eleCname;
	public ViewLead verifycName(String expectedCName)
	{
		verifyPartialText(eleCname, expectedCName);
		return this;
	}
}
