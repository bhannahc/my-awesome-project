package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName")
	WebElement eleCName;
	public CreateLead typeCompanyName(String cName)
	{
		type(eleCName, cName);
		return this;
	}
	
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement eleFName;
	public CreateLead typeFirstName(String fName)
	{
		type(eleFName, fName);
		return this;
	}
	
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement eleLName;
	public CreateLead typeLastName(String lName)
	{
		type(eleLName, lName);
		return this;
	}
	
	@FindBy(how=How.NAME,using="submitButton")
	WebElement clickLead;
	public ViewLead clickCreateLead()
	{
		click(clickLead);
		return new ViewLead();
		
	}
	
}
