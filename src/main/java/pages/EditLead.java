package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods{
	
	public EditLead()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="updateLeadForm_companyName")
	WebElement eleCName;
	public EditLead clearText()
	{
		eleCName.clear();
		return this;
	}
	
	public EditLead editCName(String cName)
	{
		type(eleCName, cName);
		return this;
	}
	
	@FindBy(how=How.NAME,using="submitButton")
	WebElement submitButton;
	public ViewLead editDone()
	{
		click(submitButton);
	    return new ViewLead(); 
	}
	
}
