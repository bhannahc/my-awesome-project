package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public FindLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME,using="firstName")
	WebElement eleFName;
	public FindLeads enterFirstName(String fName)
	{
		type(eleFName, fName);
		return this;
	}
	
	@FindBy(how=How.LINK_TEXT,using="Find Leads")
	WebElement searchButton;
	public FindLeads searchLead()
	{
		click(searchButton);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")
	WebElement eleClickResult;
	public ViewLead clickSearchResult()
	{
		click(eleClickResult);
		return new ViewLead();
	}

}
