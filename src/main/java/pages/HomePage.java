package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//h2[text()='Demo Sales Manager']")
	WebElement loginName;
	public HomePage verifyLoginName(String expectedText)
	{
		verifyExactText(loginName, expectedText);
		return this;
	}
	
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
	WebElement eleLink;
	public MyHomePage clickLink()
	{
		click(eleLink);
		return new MyHomePage();
	}
	
}
