package learnExcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	public static Object[][] readExcelData(String fileName) throws IOException {
		XSSFWorkbook wBook = new XSSFWorkbook("./Data/"+fileName+".xlsx");
		
		XSSFSheet sheet = wBook.getSheetAt(0);
		
		int rCount = sheet.getLastRowNum();
		short cCount = sheet.getRow(0).getLastCellNum();
		Object [][] data = new Object[rCount][cCount];
		/*System.out.println("Number of rows-->"+rCount);
		System.out.println("Number of columns-->"+cCount);*/
		for (int j = 1; j <= rCount; j++) {
			for (int i = 0; i < cCount; i++) {
				XSSFCell cell = sheet.getRow(j).getCell(i);
				data[j-1][i] = cell.getStringCellValue();
				
			} 
		}
		
		return data;
	}

}
