package createLead;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_CreateLead";
		testCaseDesc = "Creating a lead";
		category = "Smoke";
		author = "Hannah";
		excelName = "TC001_CreateLead";
	}
	
@Test(dataProvider = "data")
	public void createLead(String uName, String pwd, String expectedUName, String cName, String fName, String lName, String expectedfName)
	{
		new LoginPage().typeUserName(uName).typePassword(pwd).clickLogin()
		.verifyLoginName(expectedUName).clickLink()
		.clickLeads()
		.clickCreateLeadLink()
		.typeCompanyName(cName).typeFirstName(fName).typeLastName(lName).clickCreateLead()
		.verifyFName(expectedfName);
		
	
	
	}
	
}
